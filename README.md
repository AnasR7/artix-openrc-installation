# Installation of Artix Linux OpenRC

## ## Requirement ##
- Virtual Machine (KVM/QEMU or VirtualBox) / Laptop or PC
- https://artixlinux.org (for download ISO)

## 1. Setup the virtual Machine
### 1.1 Setup Installation KVM/QEMU
### 1.2 Setup Installation VirtualBox


## Installation Proccess Arti Linux

### Basic Installation
1. Login Artix login with user: artix pass: artix
2. begin to super user with `sudo su` 
3. check if you have the internet with `ping `
4. check partition mount point with `lsblk`
5. change or modify the partition with `cfdisk`
6. for format the partition we have we use `mkfs`
    - for FAT32 or GRUB =  `mkfs.fat -F32 /partition-location`
    - for Home Partition = `mkfs.ext4 /partition-location`
    - for Swap partition = `mkswap /partition-location`
7. now we mount the partition with `mount `
    - mount the Home partition
        - `mount /partition-location /mnt`

    - mount the GRUB/UEFI partition
        - create efi folder `mkdir -p /mnt/boot/efi`
        - `mount /partition-location /mnt/boot/efi`
8. mount swap partition with `swapon`
9. download & install the bases system
    - `basestrap /mnt base base-devel openrc linux linux-firmware "yourproc"-ucode etc`
10. create filesystem table with `fstabgen`
    - `fstabgen -U /mnt >> /mnt/etc/fstabgen`
    - check with `cat /mnt/etc/fstab`
11. open the system with `artix-chroot`
    - `artix-chroot /mnt`
12. create locale
    - `ln -sf /usr/share/zoneinfo/Asia/Jakarta /etc/localtime`
13. sync clock `hwclock --systohc`
14. choose locale /language with text-editor
    - `'vim/or nano' /etc/locale.gen`
15. generate locale `locale-gen`
16. create ``locale.conf`` with vim in `/etc/` and add your locale setting in locale.gen
17. create hostname in `/etc/`
18. setting host in system in `etc/hosts`
    - `127.0.0.1 localhost
      ::1 localhost
      127.0.1.1 'hostname'.localdomain`
19. create password for super user with `passwd` 
20. add user with `useradd -mG wheel 'username'`
21. add password for user `passwd 'username'`
22. give previllage for user by edit `visudo`
    - uncomment `%wheel ALL=(ALL) ALL` 
23. downlaod and install openrc, drivers, and artix package
    - `pacman -S grub efibootmgr networkmanager networkmanager-openrc network-manager-applet
      wpa_supplicant dialog os-prober dosfstools linux-headers cups cups-openrc `
24. install grub
    - `grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB`
25. create grub config.
    - `grub-mkconfig -o /boot/grub/grub.cfg`
26. update driver in OpenRC service
    - `rc-update add 'services like NetworkManager or cupsd' default`
27. and restart and done

